export interface IPais {
    id: number,
    nombre: string,
    habitantes: number,
    presidente: string,
    capital: string

}