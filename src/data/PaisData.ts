import type { IPais } from "@/interfaces/IPaises";

const PaisData: IPais[] = [
    {
        id: 1,
        nombre: 'México',
        habitantes: 1311000000,
        presidente: 'AMLO',
        capital: 'CDMX'  
    },
    {
        id: 2,
        nombre: 'Canada',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 3,
        nombre: 'Guatemala',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 4,
        nombre: 'Honduras',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 5,
        nombre: 'Salvador',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 6,
        nombre: 'Belice',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 7,
        nombre: 'Estados Unidos',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 8,
        nombre: 'España',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 9,
        nombre: 'Chile',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
    {
        id: 10,
        nombre: 'Panamá',
        habitantes: 39566248,
        presidente: 'Justin Trudeau',
        capital: 'Ottawa'  
    },
]

export default PaisData;